@extends('admin.layouts.layout')


@section('title')

    Employee

@endsection


@section('header')


@endsection


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
      <div class="clear" style="clear: both"></div>
        <h1>
            Employee
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  Main </a></li>
          <li class="active"><a href="{{url('/adminpanal/employee')}}">Employee
</a></li>
          <!-- <li class="active">Data tables</li> -->
        </ol>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Employee</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table style="width: 1000px; background-color: #f5f5f5; " id="myTable" class="table table-bordered table-hover">
                    <thead>

                    </tbody>


                      <tr>
                        <th>#</th>
                        <th>first Name</th>
                        <th> last name </th>
                        <th>job</th>
                        <th>Status</th>
                        <th>operate</th>
                         <th>show photo</th>

                          </th>


                      </tr>
                    </tbody>


                    @foreach($cor as $employee)
                    <tr>
                        <th>#</th>

                        <th>{{$employee->f_name}}</th>
                        <th>{{$employee->l_name}}</th>
                        <th>{{$employee->job}}</th>

                        @if($employee->status == 1 )
                            <th>Active</th>
                        @else
                            <th>No Active</th>

                        @endif
                        <th>
                          <a href="{{url('/adminpanal/employee/'. $employee->id.'/edit')}}">Edit </a>
                          <a href="{{url('/adminpanal/employee/'. $employee->id.'/delete')}}">Delete</a>

                        </th>

                        <th><a href="/adminpanal/employee/{{$employee->id}}"> Show</a></th>


                      </tr>
                      @endforeach

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

        @endsection



@section('footer')



<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">
    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
</script>



@endsection
