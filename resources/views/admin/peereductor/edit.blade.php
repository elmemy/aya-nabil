@extends('admin.layouts.layout')


@section('title')

Edit
{{$peereductor->name}}

@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
 <h1>

  Edit
{{$peereductor->name}}
 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main</a></li>
   <li><a href="{{url('/adminpanal/peereducator')}}">Peer Eductor</a></li>
   <li class="active"><a href="{{url('/adminpanal/peereducator/'.$peereductor->id.'/edit')}}">

    Edit
{{$peereductor->name}}
   </a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">
                  Edit
                    {{$peereductor->name}}
                 </h3>
               </div><!-- /.box-header -->
               <div class="box-body">

                 {!! Form::model($peereductor,['route'=>['peereducator.update' , $peereductor->id] , 'method'=>'PATCH']) !!}
                   @include('admin.peereductor.form')
                 {!! Form::close()!!}

           </div>
             </div>
         </div>

       </section>




@endsection



@section('footer')



@endsection
