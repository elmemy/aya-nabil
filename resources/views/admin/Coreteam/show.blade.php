@extends('admin.layouts.layout')


@section('title')

    CoreTeam
@endsection


@section('header')


@endsection


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="clear" style="clear: both"></div>
        <h1>
            CoreTeam

        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  Main </a></li>
            <li class="active"><a href="{{url('/adminpanal/coreteam')}}">coreteam
                </a></li>
            <!-- <li class="active">Data tables</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <h3 class="box-title">CoreTeam </h3>
                    <h4>صورة العضو</h4>

                    <img src="{{URL::to('/')}}/{{$cor->image}}"  alt="">

                </div><!-- /.box-header -->
            </div><!-- /.box -->

        </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

@endsection


