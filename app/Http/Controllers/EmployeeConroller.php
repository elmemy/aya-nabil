<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class EmployeeConroller extends Controller
{

    public function index()
    {
        $cor = Employee::all();
        return view ('admin.Local.index',compact('cor'));
    }


    public function create()
    {
        return view ('admin.Local.add');
    }

    public function show($id)
    {
        $cor = Employee::find($id);
        return view ('admin.Local.show',compact('cor'));
    }


    public function store(Request $request)
    {
        if($request->hasFile('image')){
            $file = $request->file('image');
            $destinationPath = 'uploads/';
            $extension = $file->getClientOriginalExtension();
            $image=str_random(10).".".$extension;
            $file->move($destinationPath , $image);
            $image = 'uploads/'.$image;
        }

        $cor = new Employee;
        $cor->f_name = $request->f_name;
        $cor->l_name = $request->l_name;
        $cor->job = $request->job;
        $cor->status = $request->status;
        $cor->user_id =  Auth::id();
        $cor->image = $image;

        $cor->save();


        return Redirect('/adminpanal/employee')->withFlashMessage('Done');
    }

    public function edit($id)
    {
        $cor =Employee::find($id);
        return view('admin.Local.edit',compact('cor'));
    }


    public function update($id ,EmployeeRequest $request)
    {
        $cor= Employee::find($id);
        $cor->fill($request->all())->save();
        return Redirect::back()->withFlashMessage('Done');

    }


    public function destroy($id)
    {
        $cor = Employee::findOrFail($id)->delete();
        return redirect('/adminpanal/employee')->withFlashMessage('Delete');
    }
}
