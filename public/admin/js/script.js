$(document).ready(function() {

    // WINDOW HEIGHT

    WinHeight();
    $(window).resize(function (){
        WinHeight();
    });

    function WinHeight() {
        $(".select2-container").css({'width':'100%'});
        $('.win-height').css({'min-height': $(window).height() - $('.top-header').height()});
        $('.side-menu').css({'min-height': $('.main-content').height()});

        if($(window).width() >= 768) {
            $('.main-content, .side-menu, .menu-btn').removeClass('close-menu');
        }else if($(window).width() < 768) {
            $('.main-content, .side-menu, .menu-btn').addClass('close-menu');
        }
    }


    // MENU BUTTON

    $('.menu-btn').click(function (){
        $('.main-content, .side-menu, .menu-btn').toggleClass('close-menu');
    });



    // MENU DROPDOWN

    $('.menu-links .drop-down-menu').slideUp(0);
    $('.menu-links .drop-down-btn.active').next('.drop-down-menu').slideDown(0);
    $('.drop-down-btn').click(function (){
        $(this).next().stop().slideToggle();
        $(this).toggleClass('active');
    });



    // SELECT 2

    $(".select_2").select2({
        dir: "rtl"
    });



    // LOAD BTN

    $('#loadBTN').on('click', function () {
        var $btn = $(this).button('loading');
        setTimeout(function() {
            $btn.button("reset")
        }, 3e3)
    });



    // TOOLTIP

    $('[data-toggle="tooltip"]').tooltip();


});