@extends('admin.layouts.layout')


@section('title')

    Trainers

@endsection


@section('header')


@endsection


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
      <div class="clear" style="clear: both"></div>
        <h1>
            Trainers
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  Main </a></li>
          <li class="active"><a href="{{url('/adminpanal/trainers')}}">Trainers
</a></li>
          <!-- <li class="active">Data tables</li> -->
        </ol>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Trainers</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table style="width: 2700px; background-color: #f5f5f5; " id="bootstrap-table2" class="table table-bordered table-hover">
                    <thead>

                    </tbody>


                      <tr>
                        <th>#</th>
                        <th>الاسم</th>
                          <th>المحافطة</th>
                          <th>تاريخ الميلاد</th>
                        <th>رقم التليفون</th>
                        <th>البريد الإلكتروني</th>
                        <th>الفيس بوك</th>
                        <th>تويتر  </th>
                        <th>طالب / خريج /دراسات عليا</th>
                        <th>الكلية</th>
                        <th>المسمي الوظيفي </th>
                        <th>متطوع في جمعية اخري</th>
                        <th>اسم الشريك </th>
                        <th>عدد سنوات الخبرة</th>
                        <th>الخبرة التطوعية الخاصة بك</th>
                          <th>تاريخ دخولك الشركة</th>
                          <th>تأثرك بالشركة</th>
                          <th> ما هي خبراتك</th>

                        <th>عمليات</th>

                          <th>عرض صورة العضو   </th>


                          </th>


                      </tr>
                    </tbody>


                    @foreach($tar as $peereductor)
                    <tr>
                        <th>#</th>

                        <th>{{$peereductor->name}}</th>
                        <th>{{$peereductor->mo7afza}}</th>
                        <th>{{$peereductor->birhday}}</th>
                        <th>{{$peereductor->tel}}</th>
                        <th>{{$peereductor->email}}</th>
                        <th>{{$peereductor->fb}}</th>
                        <th>{{$peereductor->twitter}}</th>
                        <th>{{$peereductor->choice}}</th>
                        <th>{{$peereductor->faculty}}</th>
                        <th>{{$peereductor->job}}</th>
                        <th>{{$peereductor->choice_2}}</th>
                        <th>{{$peereductor->name_sharek}}</th>
                        <th>{{$peereductor->num_5ebra}}</th>
                        <th>{{$peereductor->num_5ebra_2}}</th>
                        <th>{{$peereductor->date}}</th>
                        <th>{{$peereductor->tasor}}</th>
                        <th>{{$peereductor->num_5ebra_3}}</th>



                           <th>
                          <a href="{{url('/adminpanal/trainers/'. $peereductor->id.'/edit')}}">Edit </a>
                          <a href="{{url('/adminpanal/trainers/'. $peereductor->id.'/delete')}}">Delete</a>

                        </th>

                        <th><a href="/adminpanal/trainers/{{$peereductor->id}}"> عرض</a></th>


                      </tr>
                      @endforeach
                      {{ $tar->links() }}

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

        @endsection



@section('footer')



<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">

</script>


<script>
$(document).ready( function () {
    $('#bootstrap-table2').bdt();
});
</script>

@endsection
