<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <title>aya nabil @yield('title')</title>

    {!! Html::style('admin/css/bootstrap.min.css') !!}
    {!! Html::style('admin/css/font-awesome.min.css') !!}
    {!! Html::style('admin/css/jquery.mCustomScrollbar.min.css') !!}
    {!! Html::style('admin/css/style.css') !!}

  </head>

  <body>

  <!--===============================
      TOP HEADER
  ===================================-->

  <div class="top-header">
      <div class="clearfix">

          <div class="pull-right">
              <a class="menu-btn"><i class="fa fa-navicon"></i></a>
              <!-- <img src="{{ url('admin/images/logo.png') }}"class="logo" alt=""> -->

          </div>


          <div class="pull-left">

              <div class="dropdown">
                  <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i class="fa fa-bell-o"></i>
                  </button>
                  <div class="dropdown-menu big-menu" aria-labelledby="dropdownMenu1">
                      <div class="no-hover">الاشعارات</div>
                      <div class="inner-notif mCustomScrollbar">
                          <div class="single">
                              <a href="#"> المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز <span class="text-default"><i class="fa fa-clock-o left-fa"></i> 2/9/2016</span></a>
                          </div>
                          <div class="single">
                              <a href="#"> المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز <span class="text-default"><i class="fa fa-clock-o left-fa"></i> 2/9/2016</span></a>
                          </div>
                          <div class="single">
                              <a href="#"> المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز <span class="text-default"><i class="fa fa-clock-o left-fa"></i> 2/9/2016</span></a>
                          </div>
                          <div class="single">
                              <a href="#"> المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز <span class="text-default"><i class="fa fa-clock-o left-fa"></i> 2/9/2016</span></a>
                          </div>
                          <div class="single">
                              <a href="#"> المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز <span class="text-default"><i class="fa fa-clock-o left-fa"></i> 2/9/2016</span></a>
                          </div>
                          <div class="single">
                              <a href="#"> المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز <span class="text-default"><i class="fa fa-clock-o left-fa"></i> 2/9/2016</span></a>
                          </div>
                      </div>
                      <div class="no-hover"><a href="notifications.html">عرض الكل</a></div>
                  </div>
              </div>



              <div class="dropdown">
                  <button class="dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      {{--<img src="{{ url('admin/images/user-pic.jpg') }}"class="right-fa" alt=""> {{ Auth::user()->name }}<i class="fa fa-angle-down left-fa"></i>--}}

                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                      <li><a href="profile.html">الصفحة الشخصية</a></li>
                      <li><a href="settings.html">الاعدادات</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="/logout">تسجيل الخروج</a></li>
                  </ul>
              </div>

          </div>

      </div>
  </div>



  <div class="relative-box">


      <!--===============================
          SIDE MENU
      ===================================-->

      <div class="side-menu">

          <div class="user-info-box">
              <a href="profile.html">
                  <img src="{{ url('admin/images/user-pic.jpg') }}">

                  <div class="user-details">
                      <h3>اسم المستخدم</h3>
                      {{--<p>{{ Auth::user()->name }}</p>--}}
                  </div>
              </a>
          </div>


          <form>
              <div class="input-with-button">
                  <input type="text" placeholder="بحث..">
                  <button class="icon"><i class="fa fa-search"></i></button>
              </div>
          </form>


          <ul class="list-unstyled menu-links">

              <li><a class="drop-down-btn">الإعضاء <i class="fa fa-angle-down"></i></a>
                  <ul class="drop-down-menu">
                      <li><a href="/adminpanal/users">عرض الإعضاء</a></li>
                  </ul>
              </li>
              <li><a class="drop-down-btn">Employee<i class="fa fa-angle-down"></i></a>
                  <ul class="drop-down-menu">
                      <li><a href="/adminpanal/employee">عرض </a></li>
                      <li><a href="/adminpanal/employee/create">إضافة جديد </a></li>

                  </ul>
              </li>

          </ul>

      </div>



      <!--===============================
          MAIN CONTENT
      ===================================-->

      <div class="main-content win-height">

    @yield('content')
          <div class="container-fluid">
              <div class="clearfix">

                  </div>



              </div>
          </div>

          @yield('footer')

          <footer class="footer-fixed-bottom">
              <p>جميع الحقوق محفوظة</p>
          </footer>

      </div>


  </div>



  <!--===============================
      SCRIPT
  ===================================-->
{!!Html::script('admin/js/jquery-1.11.1.min.js')!!}
{!!Html::script('admin/js/bootstrap.min.js')!!}
{!!Html::script('admin/js/jquery.mCustomScrollbar.concat.min.js')!!}
{!!Html::script('admin/js/select2.min.js')!!}
{!!Html::script('admin/js/script.js')!!}

  </body>

</html>
