@extends('admin.layouts.layout')



@section('title')
Add User
@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
<div class="clear" style="clear: both;"></div>

  <h1>
Add User
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main </a></li>
    <li><a href="{{url('/adminpanal/users')}}">UserController</a></li>
    <li class="active"><a href="{{url('/adminpanal/users/create')}}">AddUsers</a></li>

    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>



        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Add Users</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <form class="form-horizontal" role="form" method="POST" action="{{ url('/adminpanal/users') }}">

                    @include('admin.user.form')
                  </form>
            </div>
              </div>
          </div>

        </section>
@endsection



@section('footer')


 
@endsection
