{!! csrf_field() !!}

 <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">الاسم</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("name" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('name'))
             <span class="help-block">
                 <strong>{{ $errors->first('name') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">تاريخ الميلاد</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("birthday" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('birthday'))
             <span class="help-block">
                 <strong>{{ $errors->first('birthday') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="clear" style="clear: both; padding:10px;"></div>

<div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">السن</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("age" ,null ,['class'=> 'form-control'])!!}


         @if ($errors->has('age'))
             <span class="help-block">
                 <strong>{{ $errors->first('age') }}</strong>
             </span>
         @endif
     </div>
 </div>

<!-- ـــــــــــــــــــــــــــ -->
<div class="form-group{{ $errors->has('Governorate') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">المحافظة</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("Governorate" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('Governorate'))
             <span class="help-block">
                 <strong>{{ $errors->first('Governorate') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">التليفون</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("tel" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('tel'))
             <span class="help-block">
                 <strong>{{ $errors->first('tel') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">بداية دخولة الشبكة</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("date" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('date'))
             <span class="help-block">
                 <strong>{{ $errors->first('date') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">Email</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("email" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('email'))
             <span class="help-block">
                 <strong>{{ $errors->first('email') }}</strong>
             </span>
         @endif
     </div>
 </div>


 <div class="form-group{{ $errors->has('tadreb') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">التدريبات التي حصل عليها</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("tadreb" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('tadreb'))
             <span class="help-block">
                 <strong>{{ $errors->first('tadreb') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('rkmkomy') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">الرقم القومي</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("rkmkomy" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('rkmkomy'))
             <span class="help-block">
                 <strong>{{ $errors->first('rkmkomy') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('gm3ya') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">الجمعية التابع لها</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("gm3ya" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('gm3ya'))
             <span class="help-block">
                 <strong>{{ $errors->first('gm3ya') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('mharat') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">المهارات الشخصية</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("mharat" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('mharat'))
             <span class="help-block">
                 <strong>{{ $errors->first('mharat') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('fb') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">FaceBook</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("fb" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('fb'))
             <span class="help-block">
                 <strong>{{ $errors->first('fb') }}</strong>
             </span>
         @endif
     </div>
 </div>


 <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">Twitter</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("twitter" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('twitter'))
             <span class="help-block">
                 <strong>{{ $errors->first('twitter') }}</strong>
             </span>
         @endif
     </div>
 </div>
 </div>
 <div class="clear" style="clear: both; padding:10px;"></div>
 <div class="form-group">
   {{ Form::label('صورة', 'Upload a Featured Image') }}
				{{ Form::file('image') }}




 <div class="form-group">
     <div class="col-md-6 col-md-offset-4">
         <button type="submit" class="btn btn-primary">
             <i class="fa fa-btn fa-user"></i>Add
         </button>
     </div>
 </div>
