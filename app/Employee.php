<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Employee extends Model
{
    use Notifiable;
    use SearchableTrait;
    protected $table = 'employee';

    protected $searchable = [
        'columns' => [
            'employee.f_name' => 10,
            'employee.l_name' => 10,
            'employee.job' => 10,


        ]
    ];
}
