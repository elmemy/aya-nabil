@extends('admin.layouts.layout')


@section('title')

Peer Eductor

@endsection


@section('header')


@endsection


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
      <div class="clear" style="clear: both"></div>
        <h1>
Peer Eductor

        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  Main </a></li>
          <li class="active"><a href="{{url('/adminpanal/peereducator')}}">Peer Eductor
</a></li>
          <!-- <li class="active">Data tables</li> -->
        </ol>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Peer Eductor</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table style="width: 2700px; background-color: #f5f5f5; " id="bootstrap-table2" class="table table-bordered table-hover">
                    <thead>

                    </tbody>


                      <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>تاريخ الميلاد</th>
                        <th>السن</th>
                        <th>المحافظة</th>
                        <th>التليفون</th>
                        <th>بداية دخولة الشبكة</th>
                        <th>Email</th>
                        <th>التدريبات التي حصل عليها</th>
                        <th>الرقم القومي</th>
                        <th>الجمعية التابع لها</th>
                        <th>المهارات الشخصية</th>
                        <th>FB</th>
                        <th>Twitter</th>


                        <th>عمليات</th>

                          <th>عرض صورة العضو   </th>


                          </th>


                      </tr>
                    </tbody>
                    @foreach($peereductor as $peereductors)
                    <tr>
                        <th>#</th>

                        <th>{{$peereductors->name}}</th>
                        <th>{{$peereductors->birthday}}</th>
                        <th>{{$peereductors->age}}</th>
                        <th>{{$peereductors->Governorate}}</th>
                        <th>{{$peereductors->tel}}</th>
                        <th>{{$peereductors->date}}</th>
                        <th>{{$peereductors->email}}</th>
                        <th>{{$peereductors->tadreb}}</th>
                        <th>{{$peereductors->rkmkomy}}</th>
                        <th>{{$peereductors->gm3ya}}</th>
                        <th>{{$peereductors->mharat}}</th>
                        <th>{{$peereductors->fb}}</th>
                        <th>{{$peereductors->twitter}}</th>



                           <th>
                          <a href="{{url('/adminpanal/peereducator/'. $peereductors->id.'/edit')}}">Edit </a>
                          <a href="{{url('/adminpanal/peereducator/'. $peereductors->id.'/delete')}}">Delete</a>

                        </th>

                        <th><a href="/adminpanal/peereducator/{{$peereductors->id}}"> عرض</a></th>


                      </tr>


                      @endforeach

                  </table>
                      {{ $peereductor->links() }}


                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

        @endsection



@section('footer')



<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">

</script>


<script>
$(document).ready( function () {
    $('#bootstrap-table2').bdt();
});
</script>

@endsection
