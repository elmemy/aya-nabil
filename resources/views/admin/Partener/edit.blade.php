@extends('admin.layouts.layout')


@section('title')

Edit
{{$par->name}}

@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
 <h1>

  Edit
{{$par->name}}
 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main</a></li>
   <li><a href="{{url('/adminpanal/partener')}}">Parnter Associated</a></li>
   <li class="active"><a href="{{url('/adminpanal/partener/'.$par->id.'/edit')}}">

    Edit
{{$par->name}}
   </a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">
                  Edit
                    {{$par->name}}
                 </h3>
               </div><!-- /.box-header -->
               <div class="box-body">

                 {!! Form::model($par,['route'=>['partener.update' , $par->id] , 'method'=>'PATCH']) !!}
                   @include('admin.Partener.form')
                 {!! Form::close()!!}

           </div>
             </div>
         </div>

       </section>




@endsection



@section('footer')



@endsection
