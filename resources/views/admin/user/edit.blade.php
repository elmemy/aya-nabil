@extends('admin.layouts.layout')


@section('title')

تعديل العضو
{{$user->name}}
@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
 <h1>

   تعديل العضو

   {{$user->name}}
 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>الرئيسة</a></li>
   <li><a href="{{url('/adminpanal/users')}}">الأعضاء</a></li>
   <li class="active"><a href="{{url('/adminpanal/users/'.$user->id.'/edit')}}">
     تعديل العضو


     {{$user->name}}
   </a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">
                   تعديل العضو
                   {{$user->name}}
                 </h3>
               </div><!-- /.box-header -->
               <div class="box-body">
                 {!! Form::model($user,['route'=>['users.update' , $user->id] , 'method'=>'PATCH']) !!}

                   @include('admin.user.form')
                 {!! Form::close()!!}

           </div>
             </div>
         </div>

       </section>





              <!-- Main content -->
              <section class="content">
                <div class="row">
                  <div class="col-xs-12">
                      <div class="box-header">
                        <h3 class="box-title">
تعديل كلمة المرور
                          {{$user->name}}
                        </h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">



      {!! Form::open(['url'=> '/adminpanal/user/changePassword', 'method'=>'post'] ) !!}
      <input type="hidden" value="{{$user->id}}" name="user_id">
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">كلمة المرور</label>

              <div class="col-md-6">
                <input type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
              </div>
        </div>
              <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                      <i class="fa fa-btn fa-user"></i>Edit password
                    </button>
              </div>
            </div>

            <div class="form-group">

                {!! Form::close()!!}
            </div>
                    </div>
                </div>

              </section>




@endsection



@section('footer')



@endsection
