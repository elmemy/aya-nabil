@extends('admin.layouts.layout')


@section('title')

Edit
{{$tar->name}}

@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
 <h1>

  Edit
{{$tar->name}}
 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main</a></li>
   <li><a href="{{url('/adminpanal/trainers')}}">Peer Eductor</a></li>
   <li class="active"><a href="{{url('/adminpanal/trainers/'.$tar->id.'/edit')}}">

    Edit
{{$tar->name}}
   </a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">
                  Edit
                    {{$tar->name}}
                 </h3>
               </div><!-- /.box-header -->
               <div class="box-body">

                 {!! Form::model($tar,['route'=>['trainers.update' , $tar->id] , 'method'=>'PATCH']) !!}
                   @include('admin.Trainers.form')
                 {!! Form::close()!!}

           </div>
             </div>
         </div>

       </section>




@endsection



@section('footer')



@endsection
