<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function employee()

    {
        $employe = [];
        $employee = Employee::all();
        foreach ($employee as $em)
        {
            $employe[] =[


                "f_name" =>$em->f_name,
                "l_name" =>$em->l_name,
                "image" =>$em->image,
                "job" =>$em->job,
                "status" =>$em->status,
                "user_id" =>$em->user_id,

            ];

        }

        return response()->json(['result' => $employe]);

    }

    public function search(Request $request)
    {
        if (!$request->search)
          return response()->json(['msg' => 'search_Required']);

    	if($request->has('search')){
            $users = Employee::search($request->get('search'))->get();

            
        }else{
            $users = Employee::get();
        }

      return response()->json(['result' => $users]);
    }
}
