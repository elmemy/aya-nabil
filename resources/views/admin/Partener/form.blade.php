{!! csrf_field() !!}

 <div class="form-group{{ $errors->has('gm3ya') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">اسم الجمعية</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("gm3ya" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('gm3ya'))
             <span class="help-block">
                 <strong>{{ $errors->first('gm3ya') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('mo7fza') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">المحافظة</label>

    <div class="col-md-6">
    <!-- <input type="text" class="form-control" name="name" value="{{ old('mo7afza') }}"> -->
        {!! Form::text("mo7fza" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('mo7fza'))
            <span class="help-block">
                 <strong>{{ $errors->first('mo7fza') }}</strong>
             </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">رقم التليفون </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("tel" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('tel'))
             <span class="help-block">
                 <strong>{{ $errors->first('tel') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="clear" style="clear: both; padding:10px;"></div>


<!-- ـــــــــــــــــــــــــــ -->
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">البريد الإلكتروني</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("email" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('email'))
             <span class="help-block">
                 <strong>{{ $errors->first('email') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('nshat') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label"> انشطة الجمعية</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("nshat" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('nshat'))
             <span class="help-block">
                 <strong>{{ $errors->first('nshat') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">بداية التشبيك مع الشبكة  </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("date" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('date'))
             <span class="help-block">
                 <strong>{{ $errors->first('date') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('ms2ol') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">مسؤول التواصل</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("ms2ol" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('ms2ol'))
             <span class="help-block">
                 <strong>{{ $errors->first('ms2ol') }}</strong>
             </span>
         @endif
     </div>
 </div>


 <div class="form-group{{ $errors->has('tel_ms2ol') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">   رقم تليفون المسؤول</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("tel_ms2ol" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('tel_ms2ol'))
             <span class="help-block">
                 <strong>{{ $errors->first('tel_ms2ol') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('email_ms2ol') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">البريد الإلكتروني للمسؤول  </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("email_ms2ol" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('email_ms2ol'))
             <span class="help-block">
                 <strong>{{ $errors->first('email_ms2ol') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('eshhar') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label"> رقم الإشهار  </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("eshhar" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('eshhar'))
             <span class="help-block">
                 <strong>{{ $errors->first('eshhar') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('gm3ya_shbka') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">ما تقيمه الجمعية المشتركة للشبكة </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("gm3ya_shbka" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('gm3ya_shbka'))
             <span class="help-block">
                 <strong>{{ $errors->first('gm3ya_shbka') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('cf7a') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">الصفحة الرسمية للشبكة</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("cf7a" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('cf7a'))
             <span class="help-block">
                 <strong>{{ $errors->first('cf7a') }}</strong>
             </span>
         @endif
     </div>
 </div>



 </div>
 <div class="clear" style="clear: both; padding:10px;"></div>
 <div class="form-group">
   {{ Form::label('صورة', 'Upload a Featured Image') }}
				{{ Form::file('image') }}




 <div class="form-group">
     <div class="col-md-6 col-md-offset-4">
         <button type="submit" class="btn btn-primary">
             <i class="fa fa-btn fa-user"></i>Add
         </button>
     </div>
 </div>
