@extends('admin.layouts.layout')


@section('title')

Add PeerEducator

@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
<div class="clear" style="clear: both;"></div>
 <h1>
   Add PeerEducator

 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i> Main</a></li>
   <li><a href="{{url('/adminpanal/peereducator')}}">PeerEducator</a></li>
   <li class="active"><a href="{{url('/adminpanal/peereducator/create')}}">Add PeerEducator</a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">Add PeerEducator
</h3>
               </div><!-- /.box-header -->
               <div class="box-body">
                   {!! Form::open(array('route' => 'peereducator.store', 'data-parsley-validate' => '', 'files' => true)) !!}

                 @include('admin.peereductor.form')
                 {!! Form::close() !!}

           </div>
             </div>
         </div>

       </section>
@endsection



@section('footer')



@endsection
