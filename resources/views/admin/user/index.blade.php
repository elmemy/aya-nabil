@extends('admin.layouts.layout')


@section('title')

|الإعضاء
@endsection


@section('header')



<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
@endsection


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
            <div class="clear" style="clear: both"></div>

        <h1>
        التحكم في الأعضاء
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  الرئيسة</a></li>
          <li class="active"><a href="{{url('/adminpanal/users')}}">التحكم في الأعضاء</a></li>
          <!-- <li class="active">Data tables</li> -->
        </ol>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">الإعضاء</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <table id="bootstrap-table" class="table table-bordered table-striped" cellspacing="0" width="100%">

                    <thead>

                      <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>البريد الإلكتروني</th>
                        <th>الوقت</th>
                        <th>عمليات</th>

                      </tr>
                    </thead>
                  <tbody>


                  @foreach($user as $userinfo)
                      <tr>
                        <th>{{$userinfo->id}}</th>
                        <th>{{$userinfo->name}}</th>
                        <th>{{$userinfo->email}}</th>
                        <th>{{$userinfo->created_at}}</th>

                        <th>
                          <a href="{{url('/adminpanal/users/'. $userinfo->id.'/edit')}}">تعديل </a>
                          <a href="{{url('/adminpanal/users/'. $userinfo->id.'/delete')}}">مسح</a>

                        </th>

                      </tr>
                      @endforeach

                    </tbody>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="text-center">
                            {!! $user->links() !!}
                        </div>
                      </div>
                    </div>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

        @endsection



@section('footer')


@endsection
