<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserRequestAdmin;
use Illuminate\Support\Facades\Redirect;
use Session;

class UserController extends Controller
{
    public function index(User $user)
    {
        $user=$user::Paginate(10);
        return view ('admin.user.index',compact('user'));
    }


    public function create()
    {
        return view ('admin.user.add');
    }


    public function store(AddUserRequestAdmin $request , User $user)
    {
        $user->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        return redirect('/adminpanal/users')->withFlashMessage('تمت إضافة العضو بنجاح');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit',compact('user'));
    }


    public function update($id,Request $request)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->admin = $request->admin;
        $user->save();
        Session::flash('success', 'تم تعديل العضو بنجاح');
        return redirect()->route('users.index', $user->id);
    }
    public function UpdatePassword(Request $request,User $user)
    {
        $userUpdated=$user->find($request->user_id);
        $password=bcrypt($request->password);
        $userUpdated->fill(['only'=>['password'=>$password]])->save();
        return Redirect::back()->withFlashMessage('تم تغير كلمة السر بنجاح');
    }

    public function destroy($id,User $user)
    {
        $user->find($id)->delete();
        return redirect('/adminpanal/users')->withFlashMessage('تم مسح العضو بنجاح');
    }
}
