@extends('admin.layouts.layout')



@section('title')

Edit SiteSetting
@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
  <h1>
Edit  SiteSetting
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main </a></li>
    <li class="active"><a href="{{url('/adminpanal/sitesetting')}}">Edit SiteSetting</a></li>

    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>



        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Edit SiteSetting</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <form action="{{url('/adminpanal/sitesetting')}}" method="post">

       {{ csrf_field() }}


                 @foreach($sitesetting as $setting)
                 <div class="form-group{{ $errors->has($setting->namesetting ) ? ' has-error' : '' }}">
                     <div class="col-lg-3">

                        {{ $setting->slug}} 
                     </div>


                    <div class="col-md-9">
                    @if($setting->type == 0)
                      {!! Form::text($setting->namesetting ,$setting->value ,['class'=> 'form-control'])!!}

                    @else
                      {!! Form::textarea($setting->namesetting ,$setting->value ,['class'=> 'form-control'])!!}
                                             
                    @endif


                      
                @if ($errors->has($setting->namesetting))
                  <span class="help-block">
                    <strong>{{ $errors->first($setting->namesetting) }}</strong>
                  </span>
                @endif
                    </div>
                 </div>





                 @endforeach
                 <button type="submit" name="submit"  class="btn btn-app">
                 <li class="fa fa-save"></li>
                    Save
                 </button>

                 </form>

              
                </div>
              </div>
          </div>

        </section>
@endsection



@section('footer')


 
@endsection
