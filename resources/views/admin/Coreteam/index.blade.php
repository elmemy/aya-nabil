@extends('admin.layouts.layout')


@section('title')

    CoreTeam

@endsection


@section('header')


@endsection


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
      <div class="clear" style="clear: both"></div>
        <h1>
            Coreteam
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  Main </a></li>
          <li class="active"><a href="{{url('/adminpanal/coreteam')}}">Coreteam
</a></li>
          <!-- <li class="active">Data tables</li> -->
        </ol>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">CoreTeam</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table style="width: 2700px; background-color: #f5f5f5; " id="bootstrap-table2" class="table table-bordered table-hover">
                    <thead>

                    </tbody>


                      <tr>
                        <th>#</th>
                        <th>الاسم</th>
                          <th>المحافطة</th>
                          <th>تاريخ الميلاد</th>
                        <th>رقم التليفون</th>
                        <th>البريد الإلكتروني</th>
                        <th>الفيس بوك</th>
                        <th>تويتر  </th>
                        <th>طالب / خريج /دراسات عليا</th>
                        <th>الكلية</th>
                        <th>المسمي الوظيفي </th>
                        <th>متطوع في جمعية اخري</th>
                        <th>اسم الشريك </th>
                        <th>عدد سنوات الخبرة</th>
                        <th>الخبرة التطوعية الخاصة بك</th>
                          <th>تاريخ دخولك الشركة</th>
                          <th>تأثرك بالشركة</th>
                          <th> ما هي خبراتك</th>
                          <th> اللجنة المسؤول عنها  </th>


                          <th>عمليات</th>

                          <th>عرض صورة العضو   </th>


                          </th>


                      </tr>
                    </tbody>


                    @foreach($cor as $peereductors)
                    <tr>
                        <th>#</th>

                        <th>{{$peereductors->name}}</th>
                        <th>{{$peereductors->mo7afza}}</th>
                        <th>{{$peereductors->birhday}}</th>
                        <th>{{$peereductors->tel}}</th>
                        <th>{{$peereductors->email}}</th>
                        <th>{{$peereductors->fb}}</th>
                        <th>{{$peereductors->twitter}}</th>
                        <th>{{$peereductors->choice}}</th>
                        <th>{{$peereductors->faculty}}</th>
                        <th>{{$peereductors->job}}</th>
                        <th>{{$peereductors->choice_2}}</th>
                        <th>{{$peereductors->name_sharek}}</th>
                        <th>{{$peereductors->num_5ebra}}</th>
                        <th>{{$peereductors->num_5ebra_2}}</th>
                        <th>{{$peereductors->date}}</th>
                        <th>{{$peereductors->tasor}}</th>
                        <th>{{$peereductors->num_5ebra_3}}</th>
                        <th>{{$peereductors->lgna}}</th>




                        <th>
                          <a href="{{url('/adminpanal/coreteam/'. $peereductors->id.'/edit')}}">Edit </a>
                          <a href="{{url('/adminpanal/coreteam/'. $peereductors->id.'/delete')}}">Delete</a>

                        </th>

                        <th><a href="/adminpanal/coreteam/{{$peereductors->id}}"> عرض</a></th>


                      </tr>
                      @endforeach
                      {{ $cor->links() }}

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

        @endsection



@section('footer')



<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">

</script>


<script>
$(document).ready( function () {
    $('#bootstrap-table2').bdt();
});
</script>

@endsection
