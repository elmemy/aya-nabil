@extends('admin.layouts.layout')


@section('title')

    Peer Eductor

@endsection


@section('header')


@endsection


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="clear" style="clear: both"></div>
        <h1>
            Peer Eductor

        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  Main </a></li>
            <li class="active"><a href="{{url('/adminpanal/peereducator')}}">Peer Eductor
                </a></li>
            <!-- <li class="active">Data tables</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <h3 class="box-title">Peer Eductor</h3>
                    <h4>صورة العضو</h4>

                    <img src="{{URL::to('/')}}/{{$peereductor->image}}"  alt="">


                </div><!-- /.box-header -->
            </div><!-- /.box -->

        </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

@endsection



@section('footer')



    <link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">

    </script>


    <script>
        $(document).ready( function () {
            $('#bootstrap-table2').bdt();
        });
    </script>

@endsection
