{!! csrf_field() !!}

 <div class="form-group{{ $errors->has('f_name') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">First Name</label>

     <div class="col-md-6">
         {!! Form::text("f_name" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('f_name'))
             <span class="help-block">
                 <strong>{{ $errors->first('f_name') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('l_name') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Last Name</label>

    <div class="col-md-6">
        {!! Form::text("l_name" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('l_name'))
            <span class="help-block">
                 <strong>{{ $errors->first('l_name') }}</strong>
             </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('job') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">job</label>

    <div class="col-md-6">
        {!! Form::text("job" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('job'))
            <span class="help-block">
                 <strong>{{ $errors->first('job') }}</strong>
             </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Status</label>

    <div class="col-md-6">
        <select class="form-control" name="status">
            <option value='1'>active</option>
            <option value='0'>No active</option>
        </select>
        @if ($errors->has('status'))
            <span class="help-block">
                 <strong>{{ $errors->first('status') }}</strong>
             </span>
        @endif
    </div>
</div>






 <div class="clear" style="clear: both; padding:10px;"></div>
 <div class="form-group">
   {{ Form::label('صورة', 'Upload a Featured Image') }}
				{{ Form::file('image') }}




 <div class="form-group">
     <div class="col-md-6 col-md-offset-4">
         <button type="submit" class="btn btn-primary">
             <i class="fa fa-btn fa-user"></i>Add
         </button>
     </div>
 </div>
