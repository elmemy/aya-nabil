@extends('admin.layouts.layout')


@section('title')

    Trainers

@endsection


@section('header')


@endsection


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
      <div class="clear" style="clear: both"></div>
        <h1>
            Parnter Associated
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>  Main </a></li>
          <li class="active"><a href="{{url('/adminpanal/partener')}}">Parnter Associated
</a></li>
          <!-- <li class="active">Data tables</li> -->
        </ol>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Parnter Associated</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table style="width: 2700px; background-color: #f5f5f5; " id="bootstrap-table2" class="table table-bordered table-hover">
                    <thead>

                    </tbody>


                      <tr>
                        <th>#</th>
                          <th>اسم الجمعية</th>
                          <th>المحافظة</th>
                          <th>التليفون</th>
                          <th>البريد الإلكتروني</th>
                          <th>أنشطة الجمعية او المبادرة</th>
                          <th>بداية التشبيك مع الشبكة</th>
                          <th>مسؤول التواصل</th>
                          <th>تليفونة</th>
                          <th>البريد الإلكتورني الخاص به </th>
                          <th>الإشهار</th>
                          <th>ما تقيمه الحمعية المشتركة للشبكة</th>
                          <th>الصفحة الرسمية</th>
                          <th>عمليات</th>
                          <th>عرض الصورة</th>
                      </tr>
                    </tbody>

                    @foreach($par as $peereductor)
                    <tr>
                        <th>#</th>

                        <th>{{$peereductor->gm3ya}}</th>
                        <th>{{$peereductor->mo7fza}}</th>
                        <th>{{$peereductor->tel}}</th>
                        <th>{{$peereductor->email}}</th>
                        <th>{{$peereductor->nshat}}</th>
                        <th>{{$peereductor->date}}</th>
                        <th>{{$peereductor->ms2ol}}</th>
                        <th>{{$peereductor->tel_ms2ol}}</th>
                        <th>{{$peereductor->email_ms2ol}}</th>
                        <th>{{$peereductor->eshhar}}</th>
                        <th>{{$peereductor->gm3ya_shbka}}</th>
                        <th>{{$peereductor->cf7a}}</th>




                           <th>
                          <a href="{{url('/adminpanal/partener/'. $peereductor->id.'/edit')}}">Edit </a>
                          <a href="{{url('/adminpanal/partener/'. $peereductor->id.'/delete')}}">Delete</a>

                        </th>

                        <th><a href="/adminpanal/partener/{{$peereductor->id}}"> عرض</a></th>


                      </tr>
                      @endforeach
                      {{ $par->links() }}

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

        @endsection



@section('footer')



<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">

</script>


<script>
$(document).ready( function () {
    $('#bootstrap-table2').bdt();
});
</script>

@endsection
