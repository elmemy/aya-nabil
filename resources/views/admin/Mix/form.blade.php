{!! csrf_field() !!}

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Title</label>

    <div class="col-md-6">
        <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
        {!! Form::text("title" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Body</label>

    <div class="col-md-6">
        <!-- <input type="email" class="form-control" name="email" value="{{ old('email') }}"> -->
        {!! Form::text("body" ,null ,['class'=> 'form-control'])!!}


        @if ($errors->has('body'))
            <span class="help-block">
                <strong>{{ $errors->first('body') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-user"></i>submit
        </button>
    </div>
</div>
