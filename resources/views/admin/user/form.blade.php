    {!! csrf_field() !!}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">الاسم</label>

        <div class="col-md-6">
            <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
            {!! Form::text("name" ,null ,['class'=> 'form-control'])!!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">البريد الإلكتروني</label>

        <div class="col-md-6">
            <!-- <input type="email" class="form-control" name="email" value="{{ old('email') }}"> -->
            {!! Form::text("email" ,null ,['class'=> 'form-control'])!!}


            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('admin') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">نوع العضوية</label>

        <div class="col-md-6">
            <!-- <input type="email" class="form-control" name="email" value="{{ old('email') }}"> -->
            <!-- {!! Form::text("admin" ,null ,['class'=> 'form-control'])!!} -->
            <select class="form-control" name="admin">
                <option value='1'>ادمن</option>
                <option value='0'>عادي</option>
            </select>

            @if ($errors->has('admin'))
                <span class="help-block">
                    <strong>{{ $errors->first('admin') }}</strong>
                </span>
            @endif
        </div>
    </div>

    @if(!isset($user))
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">الرقم السري</label>

        <div class="col-md-6">
            <input type="password" class="form-control" name="password">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Confirm Password</label>

        <div class="col-md-6">
            <input type="password" class="form-control" name="password_confirmation">

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>
    </div>
@endif
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-user"></i>تعديل العضو
            </button>
        </div>
    </div>
