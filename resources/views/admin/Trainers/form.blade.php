{!! csrf_field() !!}

 <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">الاسم</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("name" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('name'))
             <span class="help-block">
                 <strong>{{ $errors->first('name') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('mo7afza') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">المحافظة</label>

    <div class="col-md-6">
    <!-- <input type="text" class="form-control" name="name" value="{{ old('mo7afza') }}"> -->
        {!! Form::text("mo7afza" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('mo7afza'))
            <span class="help-block">
                 <strong>{{ $errors->first('mo7afza') }}</strong>
             </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('birhday') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">تاريخ الميلاد</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("birhday" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('birhday'))
             <span class="help-block">
                 <strong>{{ $errors->first('birhday') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="clear" style="clear: both; padding:10px;"></div>

<div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">رقم التليفون</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("tel" ,null ,['class'=> 'form-control'])!!}


         @if ($errors->has('tel'))
             <span class="help-block">
                 <strong>{{ $errors->first('tel') }}</strong>
             </span>
         @endif
     </div>
 </div>

<!-- ـــــــــــــــــــــــــــ -->
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">البريد الإلكتروني</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("email" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('email'))
             <span class="help-block">
                 <strong>{{ $errors->first('email') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('fb') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">الفيس بوك</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("fb" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('tel'))
             <span class="help-block">
                 <strong>{{ $errors->first('fb') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">تويتر  </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("twitter" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('twitter'))
             <span class="help-block">
                 <strong>{{ $errors->first('twitter') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('choice') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">طالب / خريج / دراسات عليا</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("choice" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('choice'))
             <span class="help-block">
                 <strong>{{ $errors->first('choice') }}</strong>
             </span>
         @endif
     </div>
 </div>


 <div class="form-group{{ $errors->has('faculty') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">   الكلية</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("faculty" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('faculty'))
             <span class="help-block">
                 <strong>{{ $errors->first('faculty') }}</strong>
             </span>
         @endif
     </div>
 </div>

 <div class="form-group{{ $errors->has('job') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">المسمي الوظيفي </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("job" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('job'))
             <span class="help-block">
                 <strong>{{ $errors->first('job') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('choice_2') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label"> هل انت متطوع في مبادرة او جمعية </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("choice_2" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('gm3ya'))
             <span class="help-block">
                 <strong>{{ $errors->first('choice_2') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('name_sharek') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">اسم الشريك (نعم) </label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("name_sharek" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('name_sharek'))
             <span class="help-block">
                 <strong>{{ $errors->first('name_sharek') }}</strong>
             </span>
         @endif
     </div>
 </div>



 <div class="form-group{{ $errors->has('num_5ebra') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">عدد سنوات الخبرة التطوعية او التدريبية</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("num_5ebra" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('num_5ebra'))
             <span class="help-block">
                 <strong>{{ $errors->first('num_5ebra') }}</strong>
             </span>
         @endif
     </div>
 </div>


 <div class="form-group{{ $errors->has('num_5ebra_2') ? ' has-error' : '' }}">
     <label class="col-md-4 control-label">الخبرة التطوعية الخاصة بك</label>

     <div class="col-md-6">
         <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
         {!! Form::text("num_5ebra_2" ,null ,['class'=> 'form-control'])!!}

         @if ($errors->has('twitter'))
             <span class="help-block">
                 <strong>{{ $errors->first('num_5ebra_2') }}</strong>
             </span>
         @endif
     </div>
 </div>


<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">تاريخ دخولك شبكة تثقيف اقران</label>

    <div class="col-md-6">
    <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
        {!! Form::text("date" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('date'))
            <span class="help-block">
                 <strong>{{ $errors->first('date') }}</strong>
             </span>
        @endif
    </div>
</div>




<div class="form-group{{ $errors->has('tasor') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">كيف تأثرت الشبكة بك     </label>

    <div class="col-md-6">
    <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
        {!! Form::text("tasor" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('tasor'))
            <span class="help-block">
                 <strong>{{ $errors->first('tasor') }}</strong>
             </span>
        @endif
    </div>
</div>





<div class="form-group{{ $errors->has('num_5ebra_3') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">تاريخ دخولك شبكة تثقيف اقران</label>

    <div class="col-md-6">
    <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
        {!! Form::text("num_5ebra_3" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('num_5ebra_3'))
            <span class="help-block">
                 <strong>{{ $errors->first('num_5ebra_3') }}</strong>
             </span>
        @endif
    </div>
</div>






<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">تاريخ دخولك شبكة تثقيف اقران</label>

    <div class="col-md-6">
    <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
        {!! Form::text("date" ,null ,['class'=> 'form-control'])!!}

        @if ($errors->has('twitter'))
            <span class="help-block">
                 <strong>{{ $errors->first('date') }}</strong>
             </span>
        @endif
    </div>
</div>




 </div>
 <div class="clear" style="clear: both; padding:10px;"></div>
 <div class="form-group">
   {{ Form::label('صورة', 'Upload a Featured Image') }}
				{{ Form::file('image') }}




 <div class="form-group">
     <div class="col-md-6 col-md-offset-4">
         <button type="submit" class="btn btn-primary">
             <i class="fa fa-btn fa-user"></i>Add
         </button>
     </div>
 </div>
