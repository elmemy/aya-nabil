@extends('admin.layouts.layout')


@section('title')

Edit user
{{$Mix->title}}
@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
 <h1>

Edit Mix|منوعات
<br>

{{$Mix->title}}
 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main</a></li>
   <li><a href="{{url('/adminpanal/Mix')}}">Mix Controller</a></li>
   <li class="active"><a href="{{url('/adminpanal/Mix/'.$Mix->id.'/edit')}}">

     Edit Mix|منوعات

     {{$Mix->title}}
   </a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">
Edit Mix
            {{$Mix->title}}
                 </h3>
               </div><!-- /.box-header -->
               <div class="box-body">
                 {!! Form::model($Mix,['route'=>['Mix.update' , $Mix->id] , 'method'=>'PATCH']) !!}
                   @include('admin.Mix.form')
                 {!! Form::close()!!}

           </div>
             </div>
         </div>

       </section>



            <div class="form-group">

                {!! Form::close()!!}
            </div>
                    </div>
                </div>

              </section>




@endsection



@section('footer')



@endsection
