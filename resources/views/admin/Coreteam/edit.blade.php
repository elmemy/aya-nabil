@extends('admin.layouts.layout')


@section('title')

Edit
{{$cor->name}}

@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
 <h1>

  Edit
{{$cor->name}}
 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main</a></li>
   <li><a href="{{url('/adminpanal/coreteam')}}">Core Team</a></li>
   <li class="active"><a href="{{url('/adminpanal/trainers/'.$cor->id.'/edit')}}">

    Edit
{{$cor->name}}
   </a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">
                  Edit
                    {{$cor->name}}
                 </h3>
               </div><!-- /.box-header -->
               <div class="box-body">

                 {!! Form::model($cor,['route'=>['coreteam.update' , $cor->id] , 'method'=>'PATCH']) !!}
                   @include('admin.Coreteam.form')
                 {!! Form::close()!!}

           </div>
             </div>
         </div>

       </section>




@endsection



@section('footer')



@endsection
