@extends('admin.layouts.layout')


@section('title')

منوعات |Mix
@endsection


@section('header')



<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
@endsection


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
            <div class="clear" style="clear: both"></div>

        <h1>
          Mix Controller
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i>Main</a></li>
          <li class="active"><a href="{{url('/adminpanal/Mix')}}">Mix Controller</a></li>
          <!-- <li class="active">Data tables</li> -->
        </ol>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Mix</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <table id="bootstrap-table" class="table table-bordered table-striped" cellspacing="0" width="100%">

                    <thead>

                      <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Image</th>
                        <th>time</th>
                        <th>Authentication</th>

                      </tr>
                    </thead>
                  <tbody>


                  @foreach($Mix as $mixinfo)
                      <tr>
                        <th>{{$mixinfo->id}}</th>
                        <th>{{$mixinfo->title}}</th>
                        <th>{{$mixinfo->body}}</th>
                        <th>{{$mixinfo->image}}</th>
                        <th>{{$mixinfo->created_at}}</th>

                        <th>
                          <a href="{{url('/adminpanal/Mix/'. $mixinfo->id.'/edit')}}">Edit </a>
                          <a href="{{url('/adminpanal/Mix/'. $mixinfo->id.'/delete')}}">Delete</a>

                        </th>

                      </tr>
                      @endforeach

</tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

        @endsection



@section('footer')


<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">

</script>


<script>
$(document).ready( function () {
    $('#bootstrap-table').bdt();
});
</script>

@endsection
