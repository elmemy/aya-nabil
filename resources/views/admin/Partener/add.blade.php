@extends('admin.layouts.layout')


@section('title')

Add Parnter Associated

@endsection


@section('header')

<!-- DataTables -->
<!-- {!! Html::style('admin/plugins/datatables/dataTables.bootstrap.css')!!} -->

@endsection


@section('content')
<section class="content-header">
<div class="clear" style="clear: both;"></div>
 <h1>
     Add Parnter Associated

 </h1>
 <ol class="breadcrumb">
   <li><a href="{{url('/adminpanal')}}"><i class="fa fa-dashboard"></i> Main</a></li>
   <li><a href="{{url('/adminpanal/partener')}}">Parnter Associated</a></li>
   <li class="active"><a href="{{url('/adminpanal/partener/create')}}">Add Parnter Associated</a></li>

   <!-- <li class="active">Data tables</li> -->
 </ol>
</section>



       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12">
               <div class="box-header">
                 <h3 class="box-title">Add Parnter Associated
</h3>
               </div><!-- /.box-header -->
               <div class="box-body">
                   {!! Form::open(array('route' => 'partener.store', 'data-parsley-validate' => '', 'files' => true)) !!}

                 @include('admin.Partener.form')
                 {!! Form::close() !!}

           </div>
             </div>
         </div>

       </section>
@endsection



@section('footer')



@endsection
