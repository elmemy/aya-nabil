<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>['web','admin']],function(){

Route::resource('/admins','AdminsController');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('adminpanal/users','UserController');
Route::resource('adminpanal/employee','EmployeeConroller');
    Route::get('/adminpanal/employee/{id}/delete','EmployeeConroller@destroy');

});
Route::get('/adminpanal/users/{id}/delete','UserController@destroy');
